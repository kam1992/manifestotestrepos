﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace manifestoTests.Models
{
    public class UserModel
    {
        public int User_acc_num { get; set; }
        public int User_acc_pin { get; set; }
        public int User_abl_total { get; set; }
        public int User_abl_od { get; set; }

        public int User_requested_amnt { get; set; }
    }
}