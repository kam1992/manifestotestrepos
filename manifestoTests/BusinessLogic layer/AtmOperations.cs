﻿using manifestoTests.DataLayer;
using manifestoTests.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace manifestoTests.BusinessLogic_layer
{
    public class AtmOperations : IAtmOperations
    {
        List<UserModel> userList = new List<UserModel>();
        int avbl_atm_cash = 0;

        public void initialiseATM()
        {
            ATMModel model = new ATMModel
            {

                Atm_Total_fund = 8000

            };


            avbl_atm_cash = model.Atm_Total_fund;


        }



        public void initialiseUser()
        {


            UserModel user1 = new UserModel
            {

                User_abl_od = 100,
                User_abl_total = 500,
                User_acc_num = 12345678,
                User_acc_pin = 1234


            };

            UserModel user2 = new UserModel
            {

                User_abl_od = 0,
                User_abl_total = 100,
                User_acc_num = 87654321,
                User_acc_pin = 4321


            };




            userList.Add(user1);
            userList.Add(user2);



        }

        public bool validateUser(int? accountNo , int? accountPin) {

            bool isValid = false;
            var qry = (from p in userList select p).Where(u=>u.User_acc_num ==accountNo && u.User_acc_pin == accountPin).ToList();


            if (qry.Count!=0)
            {
                isValid = true; 
            }
            else
            {
                isValid = false;
            }
         


            return isValid;


        }
        private int CalculateOverdraft(int avbleamount, int odamount) {


            int totalValue = avbleamount + odamount;

            return totalValue;
        }



        public int CheckBalance(int accountNo) {


            var total = (from p in userList select p).Where(u => u.User_acc_num == accountNo).FirstOrDefault();
            
            int total_avbl = CalculateOverdraft(total.User_abl_total, total.User_abl_od);
            return total_avbl;



        }

        public string WithDrawCash (int accountNo, int requestedAmount)
        {


            var user = (from p in userList select p).Where(u => u.User_acc_num == accountNo).FirstOrDefault();

       
            int total_avbl = CalculateOverdraft(user.User_abl_total, user.User_abl_od);
           
            string value;
            
            if (requestedAmount > avbl_atm_cash)
            {

                value = "ATM_ERR";

            }
            else
            {
                if (requestedAmount > total_avbl)
                {
                    value = "FUNDS_ERR";
                }
                else
                {
                    int newbalance = total_avbl - requestedAmount;
                    //userList.Where(w => w.User_acc_num == accountNo).ToList().ForEach(s => s.User_abl_total = newbalance);
                    value = newbalance.ToString();
                }
            }

            return value;


        }




    }
}