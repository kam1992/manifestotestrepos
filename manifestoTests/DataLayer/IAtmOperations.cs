﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using manifestoTests.BusinessLogic_layer;

namespace manifestoTests.DataLayer
{
   public interface IAtmOperations 
    {

        void initialiseATM();
        void initialiseUser();
        bool validateUser(int? accountNo, int? accountPin);
        int CheckBalance(int accountNo);
        string WithDrawCash(int accountNo, int requestedAmount);
    }
}
