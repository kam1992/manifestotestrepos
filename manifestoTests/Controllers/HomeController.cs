﻿using manifestoTests.BusinessLogic_layer;
using manifestoTests.DataLayer;
using manifestoTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace manifestoTests.Controllers
{
    public class HomeController : Controller
    {
        IAtmOperations AtmOperations = new AtmOperations();

        private int accountNo;
        private int pin;
       
        public ActionResult Index()
        {
           
            return View();
        }


        public ActionResult ViewHtmlCssTest()
        {
            return View();
        }




        [HttpGet]
        public ActionResult EnterATMView()
        {

           
            return View();
        }


        [HttpPost]
        public ActionResult EnterATMView( UserModel user)
        {

            AtmOperations.initialiseATM();
            AtmOperations.initialiseUser();

            try
            {
                accountNo = user.User_acc_num;
                pin = user.User_acc_pin;
              
                bool isValid= AtmOperations.validateUser(accountNo, pin);

                if (isValid == true)
                {
                    ViewBag.validMessage = "true";

                    int amount = AtmOperations.CheckBalance(user.User_acc_num);
                    ViewBag.amount = amount;
                }
                else
                {
                    ViewBag.error = "ACCOUNT_ERR";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception at" + ex);
                throw;
            }




            return View();
        }



        //[HttpPost]

        //public ActionResult ViewBalance(UserModel user)
        //{
        //    AtmOperations.initialiseATM();
        //    AtmOperations.initialiseUser();

           
        //    ViewBag.validMessage = "true";

         
        //   // TempData["amount"] = amount;


        //    return View("EnterATMView");

        //}

        [HttpPost]

        public ActionResult cashWithDraw(UserModel user)
        {
            AtmOperations.initialiseATM();
            AtmOperations.initialiseUser();

            string value = AtmOperations.WithDrawCash(user.User_acc_num, user.User_requested_amnt);
            ViewBag.validMessage = "true";

            ViewBag.amount = value;
            // TempData["amount"] = amount;


            return View("EnterATMView");

        }
    }
}